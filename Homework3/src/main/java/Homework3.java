import java.util.Arrays;
import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String scedule[][] = createAndFillScedule();

        while (true) {
            System.out.println("Please, input the day of the week: ");
            String day = in.nextLine();
            if (day.trim().equalsIgnoreCase("exit")) break;
            else System.out.println(printMessage(day.trim(), scedule, checkDay(day.trim(), scedule)));

        }


    }
     // check the input string
public static boolean checkDay(String day, String[][]scedule) {
        Scanner in = new Scanner(System.in);
        boolean flag = false;
        if(day.contains("change") || day.contains("reschedule")) {

            String [] ar = day.split(" ");

            if(ar.length>1) {

                for (int i = 0; i < 7; i++) {
                    if (ar[1].equalsIgnoreCase(scedule[i][0])) {
                        System.out.println("Please, enter new tasks for " + ar[1]);
                        String task = in.nextLine();
                        scedule[i][1] = task;
                        flag = true;
                    }
                }
            }


        }
        return flag;


}
//  print tasks or message
public static String printMessage(String day, String[][]scedule, boolean flag) {
        if(!flag) {
            for (int i = 0; i < 7; i++) {
               if (scedule[i][0].equalsIgnoreCase(day))
                    return "Your tasks for " + day + " : " + scedule[i][1];

            }

            return "Sorry, I don't understand you, please try again.";
        }
        return "";
}
       // create new String array and fill it
    public static String[][] createAndFillScedule(){
        String scedule [][] = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to gym";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to courses; learn new words in English";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "do home work, meet with friends";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to gym";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to cinema, rest at home";
        return  scedule;
    }
}
